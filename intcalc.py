k = 1
eq = None
print('27/1/2021 Basic integer calculator by Leonid Zhavoronkov', end='\n')
print("")
print('Choose one of the following operations: + - * / ^ or type "end" to close the calculator...', end='\n')
while k>0:

# addition------------------------------------------------------------------------------------------------------------------------------------

    op=input()
    if op == "+":
        a=int(input("Number 1: "))
        b=int(input("Number 2: "))
        eq=a+b
        print(a, " + ", b," = " , eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# subtraction---------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "-":
        a=int(input("Number 1: "))
        b=int(input("Number 2: "))
        eq=a-b
        print(a, " - ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# multiply------------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "*":
        a=int(input("Number 1: "))
        b=int(input("Number 2: "))
        eq=a*b
        print(a, " * ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# divide--------------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "/":
        a=int(input("Number 1: "))
        b=int(input("Number 2: "))
        eq=a/b
        print(a, " / ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# raising-------------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "^":
        a=int(input("Number 1: "))
        b=int(input("Number 2: "))
        eq=a**b
        print(a, " ^ ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# add memory----------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "m+":
        if eq == None:
            print('No number in memory, please choose another operation or type "end" to close the calculator...')
            continue 
        a=eq
        print("Number 1: ", eq)
        b=int(input("Number 2: "))
        eq=a+b
        print(a, " + ", b," = " , eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# sub memory----------------------------------------------------------------------------------------------------------------------------------

    elif op == "m-":
        if eq == None:
            print('No number in memory, please choose another operation or type "end" to close the calculator...')
            continue
        a=eq
        print("Number 1: ", eq)
        b=int(input("Number 2: "))
        eq=a-b
        print(a, " - ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")
        
# mult memory---------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "m*":
        if eq == None:
            print('No number in memory, please choose another operation or type "end" to close the calculator...')
            continue
        a=eq
        print("Number 1: ", eq)
        b=int(input("Number 2: "))
        eq=a*b
        print(a, " * ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# div memory----------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "m/":
        if eq == None:
            print('No number in memory, please choose another operation or type "end" to close the calculator...')
            continue
        a=eq
        print("Number 1: ", eq)
        b=int(input("Number 2: "))
        eq=a/b
        print(a, " / ", b, " = ", eq)

# raise memory--------------------------------------------------------------------------------------------------------------------------------

    elif op == "m^":
        if eq == None:
            print('No number in memory, please choose another operation or type "end" to close the calculator...')
            continue
        a=eq
        print("Number 1: ", eq)
        b=int(input("Number 2: "))
        eq=a**b
        print(a, " ^ ", b, " = ", eq)
        print('Type "end" to close the calculator or choose another operation (type "m" in the beggining of a command to use the result)...')
        print("")

# end-----------------------------------------------------------------------------------------------------------------------------------------
        
    elif op == "end":
        k = 0

# wrong---------------------------------------------------------------------------------------------------------------------------------------
        
    else:
        print('Wrong command, please, choose one of the following operations: + - * / ^ or type "end" to close the calculator')
