text=input("Type anything: ")
n=len(text)
s=0                                     #counts amount of spaces needed            
i=-1                                    #symbol position in the string
k=0                                     #not to lose the amount of letters in the inner loop
l=0                                     #extra variable, no practical use yet
while n>0:
    i=i+1
    k=n
    while n>0:                                                  #upper-left part
        print(text[i], end=" "),
        n=n-1
    while s>0:                          #spaces between upper parts
        print("", end="    ")
        s=s-1
    s=l
    n=k
    while n>0:                                                  #upper-right part
        print(text[i], end=" ")
        n=n-1
    s=s+1
    l=s
    print("")
    n=k-1
n=len(text)                             #resets the symbol count
m=n                                     #loop count
q=n-1
i=n
s=0
l=0
#l=n-1
while m>0:
    m=m-1
    i=i-1
    k=m
    while m<n:                                                  #bottom-left part
        print(text[i], end=" ")
        m=m+1
    m=k
    while s<n-1:                        #spaces between bottom parts
        print("", end="    ")
        s=s+1
    s=l
    while m<n:                                                  #bottom-right part
        print(text[i], end=" ")
        m=m+1
    m=k
    s=s+1
    l=s
    print(" ")
    
input('\nPress "Enter" to close the program...')
