
# in this example without "r" the text will be printed like if "\n" was used to jump to the next line

S=r'C:\newt.txt'
print(S)

# just testing the bell out

Q='\nreallylongword \a'
print(Q)

# now for the backspace *this one is obvious, i know, just want to see it*

B='\niamtetingthe\backspacehere'
print(B)

# return

R='\nlookingatthe\returnfeatures'
print(R)

# horizontal tab

H='\nsometex\tawanttotabulate'
print(H)

# vertical tab

V='\nanotherbunchoftextto\verticallytabulate'
print(V)

# and i decided to try to kill my laptop

i=1
k=1
while k>0:  
    string=("spam")
    for n in string:
        i=i+1
        string=string*i
    print(string)

